require(['jquery', 'jqueryui'], function($, jqui) {
    // JQuery is available via $
    // JQuery UI is available via $.ui

    var toggleCompletion = $('.togglecompletion');
    function checkCompleted() {
        var certificate = $('.certificate');
        var countCompletedCourses = 0;
        toggleCompletion.each(function(){
            var checkboxItemValue = $(this).find('input[name ="completionstate"]').attr('value');
            countCompletedCourses += checkboxItemValue;
        });
        if(countCompletedCourses > 0) {
            certificate.hide();
        } else {
            certificate.show();
        }
    }
    toggleCompletion.on('click', function() {
        setTimeout(function(){ checkCompleted(); }, 500);
    });
    $(document).ready(function(){
        checkCompleted();
    });
    $(document).ready(function(){
        var uid = $('#nav-notification-popover-container').attr('data-userid');
        $('.lang-en .card.activity-navigation a').text('POST-COURSE ASSESSMENT');
        $('.lang-en .card.activity-navigation a').attr('href', 'https://wellcentral.ca/vc/courses/well-being/assessment/?a=2&u=' + uid);
        $('.lang-en .card.activity-navigation a').attr('class', 'btn btn-primary');
    });
});
