<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles viewing a certificate
 *
 * @package    mod_certificate
 * @copyright  Mark Nelson <markn@moodle.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once("../../config.php");
//require_once("$CFG->dirroot/mod/certificate/locallib.php");
//require_once("$CFG->dirroot/mod/certificate/deprecatedlib.php");
//require_once("$CFG->libdir/pdflib.php");
global $CFG, $DB, $USER;
//$id = required_param('id', PARAM_INT);    // Course Module ID
$action = optional_param('action', '', PARAM_RAW);
//echo $action;die;
$file_url = $CFG->dataroot.'/filedir/'.$action;
			header('Content-Type: application/pdf');
			header("Content-Transfer-Encoding: Binary");
			header("Content-disposition: attachment; filename=".$action);
			readfile($file_url);
//$edit = optional_param('edit', -1, PARAM_BOOL);

