<?php

// This file is part of the Certificate module for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A4_non_embedded certificate type
 *
 * @package    mod_certificate
 * @copyright  Mark Nelson <markn@moodle.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();
global $CFG,$DB;
$pdf = new PDF($certificate->orientation, 'mm', 'A4', true, 'UTF-8', false);

$pdf->SetTitle($certificate->name);
$pdf->SetProtection(array('modify'));
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
$pdf->SetAutoPageBreak(false, 0);
$pdf->AddPage();

// Define variables
// Landscape
if ($certificate->orientation == 'L') {
    $x = 50;
    $y = 30;
    $sealx = 230;
    $sealy = 150;
    $sigx = 47;
    $sigy = 155;
    $custx = 47;
    $custy = 155;
    $wmarkx = 40;
    $wmarky = 31;
    $wmarkw = 212;
    $wmarkh = 148;
    $brdrx = 0;
    $brdry = 0;
    $brdrw = 297;
    $brdrh = 210;
    $codey = 175;
} else { // Portrait
    $x = 50;
    $y = 30;
    $sealx = 150;
    $sealy = 220;
    $sigx = 30;
    $sigy = 230;
    $custx = 30;
    $custy = 230;
    $wmarkx = 26;
    $wmarky = 58;
    $wmarkw = 158;
    $wmarkh = 170;
    $brdrx = 0;
    $brdry = 0;
    $brdrw = 210;
    $brdrh = 297;
    $codey = 250;
}
$coursecomp = $DB->get_record('course_completions', array('course' => $course->id, 'userid' => $USER->id));
// Add images and lines
certificate_print_image($pdf, $certificate, CERT_IMAGE_BORDER, $brdrx, $brdry, $brdrw, $brdrh);
certificate_draw_frame($pdf, $certificate);
// Set alpha to semi-transparency
$pdf->SetAlpha(0.2);
//certificate_print_image($pdf, $certificate, CERT_IMAGE_SEAL, $wmarkx, $wmarky, $wmarkw, $wmarkh);
$pdf->SetAlpha(1);
//certificate_print_image($pdf, $certificate, CERT_IMAGE_SEAL, $sealx, $sealy, '', '');
//certificate_print_image($pdf, $certificate, CERT_IMAGE_SIGNATURE, $sigx, $sigy, '', '');

// Add text
$pdf->SetTextColor(0, 0, 120);
//certificate_print_text($pdf, $x, $y, 'C', 'Helvetica', '', 30, get_string('title', 'certificate'));
$pdf->SetTextColor(0, 0, 0);
//certificate_print_text($pdf, $x, $y + 20, 'C', 'Times', '', 18, get_string('awardedto', 'certificate'));
certificate_print_text($pdf, $x + 80, $y + 77, 'L', 'Helvetica', '', 24, fullname($USER));
//certificate_print_text($pdf, $x, $y + 55, 'C', 'Helvetica', '', 18, get_string('assessment', 'certificate').' '.format_string($course->shortname).' '.get_string('international', 'certificate'));
certificate_print_text($pdf, $x + 60, $y + 100, 'L', 'Helvetica', '', 22, format_string($course->fullname));
if(!empty($coursecomp->timecompleted)){
	certificate_print_text($pdf, 75, $y + 135, 'L', 'Helvetica', '', 16, userdate($coursecomp->timecompleted,'%B %d, %Y'));
}else{
	certificate_print_text($pdf, 75, $y + 135, 'L', 'Helvetica', '', 16, userdate(time(),'%B %d, %Y'));
}

if ($course->enddate >0) {
//certificate_print_text($pdf, $x, $y + 98, 'C', 'Times', '', 10, get_string('expires', 'certificate').''. userdate(strtotime('+4 years'),'%B %d, %Y'));
}
//certificate_print_text($pdf, $x, $y + 104, 'C', 'Times', '', 10,  format_string($course->shortname) .' '.get_string('number', 'certificate').' '.$USER->profile['ipmox']);
//certificate_print_text($pdf, $x, $y + 109, 'C', 'Times', '', 10,  get_string('registry', 'certificate').' '. $USER->profile['code']);

certificate_print_text($pdf, $x, $y + 102, 'C', 'Times', '', 10, certificate_get_grade($certificate, $course));
certificate_print_text($pdf, $x, $y + 112, 'C', 'Times', '', 10, certificate_get_outcome($certificate, $course));
if ($certificate->printhours) {
    certificate_print_text($pdf, $x, $y + 122, 'C', 'Times', '', 10, get_string('credithours', 'certificate') . ': ' . $certificate->printhours);
}
//certificate_print_text($pdf, $x, $codey, 'C', 'Times', '', 10, certificate_get_code($certificate, $certrecord));
//certificate_print_text($pdf, $x, $y + 155, 'C', 'Times', '', 18, get_string('Officers', 'certificate'));
$i = 0;
if ($certificate->printteacher) {
    $context = context_module::instance($cm->id);
    if ($teachers = get_users_by_capability($context, 'mod/certificate:printteacher', '', $sort = 'u.lastname ASC', '', '', '', '', false)) {
        foreach ($teachers as $teacher) {
            $i++;
            certificate_print_text($pdf, $sigx, $sigy + ($i * 4), 'L', 'Times', '', 12, fullname($teacher));
        }
    }
}

certificate_print_text($pdf, $custx, $custy, 'L', null, null, null, $certificate->customtext);
