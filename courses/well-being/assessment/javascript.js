var questions = [
    'start',
    'I’ve been feeling optimistic about the future',
    'I’ve been feeling useful',
    'I’ve been feeling relaxed',
    'I’ve been feeling interested in other people',
    'I’ve had energy to spare',
    'I’ve been dealing with problems well',
    'I’ve been thinking clearly',
    'I’ve been feeling good about myself',
    'I’ve been feeling close to other people',
    'I’ve been feeling confident',
    'I’ve been able to make up my own mind about things',
    'I’ve been feeling loved',
    'I’ve been interested in new things',
    'I’ve been feeling cheerful'
];

var step = 1;
var answers = [];

document.addEventListener("DOMContentLoaded", function(event) {
    var url = window.location.href;
    url = url.split('/');

    if(url[url.length-2] == 'score'){
        graphic();
    }
    else {
        setStep();
    }

    var number = document.getElementsByClassName('number');
    var list = document.getElementsByClassName('q-list');
    for (var i = 0; i < questions.length; i++) {
        number[i].setAttribute('data-title', questions[i + 1]);
        list[i].innerHTML = (i + 1) + '. ' + questions[i + 1];
    }
});

function choose(id) {
    $('#but-next').addClass('active');
    if(answers[step]) $('#five' + answers[step]).removeClass('active');
    answers[step] = parseInt(id[4]);
    $('#' + id).addClass('active');
    $('#step' + step).addClass('active');
}

function next() {
    step++;
    if(step == 14){
        setStep();
        $('#but-next').text('FINISH');
        var count = 0;
        for(var i = 1; i <= 14; i++){
            if (answers[i]) count++;
        }
        if(count < 11){
            $('#but-next').removeClass('active');
        }
        else{
            $('#but-next').addClass('active');
        }
    }
    else if(step == 15){
        for(var i = 1; i <= 14; i++){
            if (!answers[i]) answers[i] = 6;
        }
        var url = window.location.href;
        if($('#checkbox-res')[0].checked == true){
            answers[15] = 1;
        }
        else{
            answers[15] = 2;
        }
        url = url + '&r1=' + answers[1] + '&r2=' + answers[2] + '&r3=' + answers[3] + '&r4=' + answers[4] + '&r5=' + answers[5] + '&r6=' + answers[6] + '&r7=' + answers[7] + '&r8=' + answers[8] + '&r9=' + answers[9] + '&r10=' + answers[10] + '&r11=' + answers[11] + '&r12=' + answers[12] + '&r13=' + answers[13] + '&r14=' + answers[14] + '&r15=' + answers[15];
        window.location.replace(url);
    }
    else{
        setStep();
    }
}

function redir(user,assess) {
    var url = window.location.href;
    url = url.split('?')[0];
    url = url + '?u=' + user + '&a=' + assess;
    window.location.replace(url);
}

function back() {
    step--;
    if(step == 13){
        $('#but-next').text('NEXT');
        $('#but-next').addClass('active');
    }
    setStep();
}

function setStep() {
    $('.five').removeClass('active');
    $('.side-left').text(questions[step]);
    if(answers[step]){
        $('#five' + answers[step]).addClass('active');
        //$('#but-next').addClass('active');
    }
    /*
    else{
        $('#but-next').removeClass('active');
    }
    */
    if(step > 1) $('#but-back').addClass('active'); else $('#but-back').removeClass('active');
}

function graphic() {
    $('#hh').css('display', 'none');
    $('#complete').css('display', 'none');
    $('#steps').css('display', 'none');
    $('#answers').css('display', 'none');
    $('#but-back').css('display', 'none');
    $('#but-next').css('display', 'none');
    $('#below').css('display', 'none');
    $('#below2').css('display', 'block');
    $('#below2').css('text-align', 'left');
    $('#results').css('display', 'block');
    $('#close').css('display', 'block');

    var canvas = document.getElementById('canvas');
    if (canvas.getContext) {
        var ctx = canvas.getContext('2d');
        ctx.beginPath();
        ctx.lineWidth = "1";
        ctx.strokeStyle = '#E4E4E4';

        for(y = 60; y <= 360; y = y+60) {
            ctx.moveTo(0, y);
            ctx.lineTo(840, y);
        }

        for(x = 60; x <= 840; x = x+60) {
            ctx.moveTo(x, 0);
            ctx.lineTo(x, 360);
        }

        ctx.stroke();

        ctx.beginPath();
        ctx.lineWidth = "5";
        ctx.strokeStyle = '#10C6C5';
        ctx.moveTo(0, 360);
        ctx.lineTo(840, 360);

        ctx.moveTo(0, 0);
        ctx.lineTo(0, 360);

        ctx.stroke();
    }
}

function notFound() {
    var url = window.location.href;
    if(url.indexOf('&c=1')>0){
        url = url.replace('&c=1', '');
        window.location.replace(url);
    }

    $('#hh').css('display', 'none');
    $('#complete').css('display', 'none');
    $('#steps').css('display', 'none');
    $('#answers').css('display', 'none');
    $('#but-back').css('display', 'none');
    $('#but-next').css('display', 'none');
    $('#below').css('display', 'none');
    $('#below2').css('display', 'block');
    $('#below2').text('Not Found');
    $('#below2').css('text-align', 'center');
    $('#results').css('display', 'none');
    $('#close').css('display', 'none');
}

function drawLine(r0,r1,r2,r3,r4,r5,r6,r7,r8,r9,r10,r11,r12,r13,r14) {
    var answers = [];
    answers[0] = r0;
    answers[1] = r1;
    answers[2] = r2;
    answers[3] = r3;
    answers[4] = r4;
    answers[5] = r5;
    answers[6] = r6;
    answers[7] = r7;
    answers[8] = r8;
    answers[9] = r9;
    answers[10] = r10;
    answers[11] = r11;
    answers[12] = r12;
    answers[13] = r13;
    answers[14] = r14;
    var score = 0;
    var missed = 0;
    for (var i = 1; i<=14; i++){
        if(answers[i] == 6){
            missed++;
        }
        else{
            score += answers[i];
        }
    }

    if(missed > 0) score = Math.round(score * 14 / (14 - missed) );

    score = score.toFixed(2);

    var canvas = document.getElementById('canvas');
    if (canvas.getContext) {
        var ctx = canvas.getContext('2d');
        ctx.beginPath();
        ctx.lineWidth = "3";
        if(answers[0] == 2) {
            $('#second-score').text(score);
            $('#second').css('display', 'block');
            ctx.strokeStyle = '#B2D235';
            ctx.fillStyle = '#B2D235';
        }
        else {
            $('#first-score').text(score);
            ctx.strokeStyle = '#00B0AF';
            ctx.fillStyle = '#00B0AF';
        }

        var x;
        var y;
        for(i = 1; i < 15; i++){
            x = i*60;
            if(answers[i] != 6){
                y = 360 - answers[i]*60;
            }
            else{
                y = 360;
            }

            ctx.arc(x, y, 3, 0, 2 * Math.PI);
            ctx.fill();
            ctx.moveTo(x, y);
        }
        ctx.stroke();
    }
}

function closeData() {
    var url = window.location.href;
    url = url + '&c=1';
    window.location.replace(url);
}

function openPopup() {
    $('#black-cover').css('display', 'flex');
}

function closePopup() {
    $('#black-cover').css('display', 'none');
}