<!DOCTYPE html>
<html lang="EN">
<head>
    <link rel="stylesheet" href="../style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="shortcut icon" href="../favicon.png" type="image/x-icon" />
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="../javascript.js"></script>
    <title>Assessment Tool</title>
</head>
<body>

<?php
error_reporting(0);

$lines = file('../data', FILE_IGNORE_NEW_LINES);
$lines = array_unique($lines);

$countPassed1 = 0;
$countPassed2 = 0;
$countNotPassed1 = 0;
$countNotPassed2 = 0;
$array1 = [];
$array2 = [];
$average = [];

foreach ($lines as $line) {
    $record = explode(",", $line);

    if($record[1] == 1){
        if($record[2] != 9 && $record[16] == 1){
            $countPassed1++;
            array_push($average, $record);
        }
        array_push($array1, $record[0]);
    }
    $countNotPassed1 = count(array_unique($array1)) * 2 - count($array1);

    if($record[1] == 2){
        if($record[2] != 9 && $record[16] == 1){
            $countPassed2++;
            array_push($average, $record);
        }
        array_push($array2, $record[0]);
    }
    $countNotPassed2 = count(array_unique($array2)) * 2 - count($array2);
}

$result1 = [];
$result2 = [];
$countReallyPassed1 = [];
$countReallyPassed2 = [];

for($i = 1; $i <= 14; $i++){
    $result1[$i] = 0;
    $result2[$i] = 0;
    $countReallyPassed1[$i] = $countPassed1;
    $countReallyPassed2[$i] = $countPassed2;
}

foreach ($average as $ave){
    if($ave[1] == 1){
        for($i = 1; $i <= 14; $i++){
            if ($ave[$i + 1] == 6){
                $countReallyPassed1[$i]--;
                continue;
            }
            $result1[$i] += $ave[$i + 1];
        }
    }
    if($ave[1] == 2){
        for($i = 1; $i <= 14; $i++){
            if ($ave[$i + 1] == 6){
                $countReallyPassed2[$i]--;
                continue;
            }
            $result2[$i] += $ave[$i + 1];
        }
    }
}

for($i = 1; $i <= 14; $i++){
    if ($countReallyPassed1[$i] > 0){
        $result1[$i] = $result1[$i] / $countReallyPassed1[$i];
    }
    else{
        $result1[$i] = 6;
    }

    if ($countReallyPassed2[$i] > 0){
        $result2[$i] = $result2[$i] / $countReallyPassed2[$i];
    }
    else{
        $result2[$i] = 6;
    }
}

$countPassedPercent1 = round(100 * $countPassed1 / ($countPassed1 + $countNotPassed1), 2);
$countPassedPercent2 = round(100 * $countPassed2 / ($countPassed2 + $countNotPassed2), 2);

echo "<script>document.addEventListener('DOMContentLoaded', function(event) {
    drawLine(1,$result1[1],$result1[2],$result1[3],$result1[4],$result1[5],$result1[6],$result1[7],$result1[8],$result1[9],$result1[10],$result1[11],$result1[12],$result1[13],$result1[14]);
    drawLine(2,$result2[1],$result2[2],$result2[3],$result2[4],$result2[5],$result2[6],$result2[7],$result2[8],$result2[9],$result2[10],$result2[11],$result2[12],$result2[13],$result2[14]);
});</script>";

?>

<div class="wrap">

    <div class="site-index">
        <div class="jumbotron container">
            <a href="https://cmhawellcentral.dev.symbicore.com/" target="_blank"><img src="../Well_Central_Logo.png" alt="Well Central Logo"></a>

            <?php
            session_start();

            if(isset($_POST['submit_pass']) && $_POST['pass'])
            {
                $pass=$_POST['pass'];
                if($pass=="well2020")
                {
                    $_SESSION['password']=$pass;
                }
                else
                {
                    $error="Incorrect Password";
                }
            }

            if(isset($_POST['page_logout']))
            {
                unset($_SESSION['password']);
            }
            ?>

            <?php
            if($_SESSION['password']=="well2020")
            {
                ?>

            <br>
            <br>
            <br>

            <div class="below beleft" >
                <h4 style="color: #00b0af;">First Assessment</h4>
                Completion: <span style="font-weight: bold;"><?php echo $countPassed1; ?></span><br>
                Completion %: <span style="font-weight: bold;"><?php echo $countPassedPercent1; ?></span><br>
                Incompletion: <span style="font-weight: bold;"><?php echo $countNotPassed1; ?></span><br>
                Incompletion %: <span style="font-weight: bold;"><?php echo 100 - $countPassedPercent1; ?></span>
            </div>

            <div class="below beleft" >
                <h4 style="color: #00b0af;">Second Assessment</h4>
                Completion: <span style="font-weight: bold;"><?php echo $countPassed2; ?></span><br>
                Completion %: <span style="font-weight: bold;"><?php echo $countPassedPercent2; ?></span><br>
                Incompletion: <span style="font-weight: bold;"><?php echo $countNotPassed2; ?></span><br>
                Incompletion %: <span style="font-weight: bold;"><?php echo 100 - $countPassedPercent2; ?></span>
            </div>

        </div>

        <div id="results" class="container" style="margin-top: 60px;">
            <div class="points">
                <div class="point">
                    <span class="point-sq">5</span>All of the time
                </div>
                <div class="point">
                    <span class="point-sq">4</span><span class="low">Often</span>
                </div>
                <div class="point">
                    <span class="point-sq">3</span>Some of the time
                </div>
                <div class="point">
                    <span class="point-sq">2</span><span class="low">Rarely</span>
                </div>
                <div class="point">
                    <span class="point-sq">1</span>None of the time
                </div>
            </div>
            <div class="graphic">
                <div style="clear: both;">
                    <canvas id="canvas" width="844px" height="360px" style="clear: both;"></canvas>
                </div>
                <div class="numbers">
                    <div class="number">1</div>
                    <div class="number">2</div>
                    <div class="number">3</div>
                    <div class="number">4</div>
                    <div class="number">5</div>
                    <div class="number">6</div>
                    <div class="number">7</div>
                    <div class="number">8</div>
                    <div class="number">9</div>
                    <div class="number">10</div>
                    <div class="number">11</div>
                    <div class="number">12</div>
                    <div class="number">13</div>
                    <div class="number">14</div>
                </div>
                <div class="please">Please place your cursor over the question number to see the associated question.</div>
                <div class="results">
                    <div class="res" id="first"><span class="res-sq"></span>First test result: <span id="first-score"></span></div>
                    <div class="res" id="second"><span class="res-sq"></span>Second test result: <span id="second-score"></span></div>
                </div>

                <div style="margin-top: 150px;" class="q-list"></div>
                <div class="q-list"></div>
                <div class="q-list"></div>
                <div class="q-list"></div>
                <div class="q-list"></div>
                <div class="q-list"></div>
                <div class="q-list"></div>
                <div class="q-list"></div>
                <div class="q-list"></div>
                <div class="q-list"></div>
                <div class="q-list"></div>
                <div class="q-list"></div>
                <div class="q-list"></div>
                <div class="q-list"></div>

            </div>
        </div>

        <div class="container" id="close">
            <form method="post" action="" id="logout_form">
                <input type="submit" class="log-btn active" id="but-close" name="page_logout" value="LOGOUT">
            </form>
        </div>

    </div>
</div>

<br>
<br>

    <div class="container">
        <p class="below" style="font-size: 14px !important;">The Warwick-Edinburgh Wellbeing Scale or [WEMWBS] was developed by the University of Warwick in conjunction with NHS Health Scotland, University of Edinburgh and the University of Leeds. ©University of Warwick, 2006, all rights reserved.</p>
    </div>

    <footer class="foo">
        <div class="cont1200">
            <div class="col1">
                <img src="../Logo_footer_1.png" alt="Well Central Logo" style="max-width: 335px">
            </div>
            <div class="col2">
                <img src="../Logo_footer_2.svg" alt="Well Central Logo" style="max-width: 335px">
            </div>
            <div class="col3">
                <ul>
                    <li><a href="https://wellcentral.ca/" target="_blank">ABOUT</a></li>
                    <li><a href="https://wellcentral.ca/well-being-course/" target="_blank">COURSE</a></li>
                    <li><a href="https://wellcentral.ca/faqs/" target="_blank">FAQ</a></li>
                    <li><a href="https://wellcentral.ca/contact/" target="_blank">CONTACT</a></li>
                </ul>
                <div class="col3b">
                    <a href="https://symbicore.com/" target="_blank">Designed and Developed by Symbicore</a>
                    <span style="padding: 0 20px; color: white; font-weight: bold">||</span>
                    <a style="cursor: pointer;" onclick="openPopup();">Disclaimer</a>
                </div>
            </div>
        </div>
    </footer>

    <div id="black-cover">
        <div class="wc-popup">
            <div id="wc-close-popup" onclick="closePopup();">+</div>
            <h1>Disclaimer:</h1>
            <p>
                This site provides general information only and may or may not reflect the position of the Canadian Mental Health Association (CMHA). Information provided is not a substitute for professional advice. If you feel that you may need medical advice, please consult a qualified health care professional. CMHA makes every reasonable effort to ensure that the information is accurate at the time of posting. We cannot guarantee the reliability of any information posted.
            </p>
        </div>
    </div>


        <?php
        }
        else
        {
            ?>
            <form method="post" action="" id="login_form">
                <h1>LOGIN TO PROCEED</h1>
                <input type="password" name="pass" placeholder="*******">
                <input type="submit" name="submit_pass" value="LOGIN">
                <p><font style="color:red;"><?php echo $error;?></font></p>
            </form>

        <?php
        }
        ?>



</body>
</html>