<!DOCTYPE html>
<html lang="EN">
<head>
    <link rel="stylesheet" href="../style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="shortcut icon" href="../favicon.png" type="image/x-icon" />
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="javascript.js"></script>
    <title>Assessment Tool</title>
</head>
<body>
<?php
error_reporting(0);
//file_put_contents('data', "\n");
if (!empty($_GET['u']) && !empty($_GET['a'])){
    $user = $_GET['u'];
    $assess = $_GET['a'];

    /*
    if(!empty($_GET['c'])){
        if ($_GET['c'] == 1){
            file_put_contents('data', "$user,$assess,9\n", FILE_APPEND | LOCK_EX);
        }
    }
    */

    if($assess == 1 || $assess == 2) {

        $passed = false;
        $passed1 = false;

        $lines = file('../data', FILE_IGNORE_NEW_LINES);
        $result = [];
        foreach ($lines as $line) {
            $record = explode(",", $line);
            if ($record[0] == $user) $result[] = $record;
        }

        foreach ($result as $res) {

            if($res[1] == 1 && $assess == 2 && $res[2] != 9){
                $passed1 = true;
            }

            if($res[1] == 1 && $assess == 2 && $res[2] != 9){
                $res1 = $res;
            }
            if ($res[2] != 9 && $res[1] == $assess) {
                $passed = true;

                if($assess == 2){
                    $res2 = $res;
                    $res = $res1;
                    echo "<script>document.addEventListener('DOMContentLoaded', function(event) {graphic();drawLine($res[1],$res[2],$res[3],$res[4],$res[5],$res[6],$res[7],$res[8],$res[9],$res[10],$res[11],$res[12],$res[13],$res[14],$res[15]);});</script>";
                    $res = $res2;
                    echo "<script>document.addEventListener('DOMContentLoaded', function(event) {drawLine($res[1],$res[2],$res[3],$res[4],$res[5],$res[6],$res[7],$res[8],$res[9],$res[10],$res[11],$res[12],$res[13],$res[14],$res[15]);});</script>";
                }
                if($assess == 1) {
                    echo "<script>document.addEventListener('DOMContentLoaded', function(event) {graphic();drawLine($res[1],$res[2],$res[3],$res[4],$res[5],$res[6],$res[7],$res[8],$res[9],$res[10],$res[11],$res[12],$res[13],$res[14],$res[15]);});</script>";
                }
            }
            /*
            if ($res[2] == 9 && $res[1] == $assess) {
                $passed = true;
                echo '<script>document.addEventListener("DOMContentLoaded", function(event) {notFound();});</script>';
            }
            */
        }

        if ($passed == false && !empty($_GET['r1'])) {
            $r0 = $assess;
            $r1 = $_GET['r1'];
            $record = "$user,$r0,$r1";
            if (!empty($_GET['r2']) && !empty($_GET['r3']) && !empty($_GET['r4']) && !empty($_GET['r5']) && !empty($_GET['r6']) && !empty($_GET['r7']) && !empty($_GET['r8']) && !empty($_GET['r9']) && !empty($_GET['r10']) && !empty($_GET['r11']) && !empty($_GET['r12']) && !empty($_GET['r13']) && !empty($_GET['r14']) && !empty($_GET['r15'])) {
                $r2 = $_GET['r2'];
                $r3 = $_GET['r3'];
                $r4 = $_GET['r4'];
                $r5 = $_GET['r5'];
                $r6 = $_GET['r6'];
                $r7 = $_GET['r7'];
                $r8 = $_GET['r8'];
                $r9 = $_GET['r9'];
                $r10 = $_GET['r10'];
                $r11 = $_GET['r11'];
                $r12 = $_GET['r12'];
                $r13 = $_GET['r13'];
                $r14 = $_GET['r14'];
                $r15 = $_GET['r15'];
                $record .= ",$r2,$r3,$r4,$r5,$r6,$r7,$r8,$r9,$r10,$r11,$r12,$r13,$r14,$r15";
            }
            $record .= "\n";
            file_put_contents('../data', $record, FILE_APPEND | LOCK_EX);
            echo "<script>document.addEventListener('DOMContentLoaded', function(event) {redir('$user','$assess');});</script>";
        }
    }
    else{
        echo '<script>document.addEventListener("DOMContentLoaded", function(event) {notFound();});</script>';
    }
    //$record = "gh@df.com,1,1,3,3,1,2,1,2,2,3,1,4,5,3,2\n";
    //file_put_contents('data', ' ');
    //file_put_contents('data', $record, FILE_APPEND | LOCK_EX);
}
else{
    echo '<script>document.addEventListener("DOMContentLoaded", function(event) {notFound();});</script>';
}

if($passed1 == false && $assess == 2){
    echo '<script>document.addEventListener("DOMContentLoaded", function(event) {notFound();});</script>';
}
else{
    file_put_contents('../data', "$user,$assess,9\n", FILE_APPEND | LOCK_EX);
}


?>

<div class="wrap">

    <div class="site-index">
        <div class="jumbotron container">
            <a href="https://cmhawellcentral.dev.symbicore.com/" target="_blank"><img src="Well_Central_Logo.png" alt="Well Central Logo"></a>
            <h1 id="hh">L'échelle de bien-être mental Warwick-Edinburgh (WEMWBS)</h1>

            <p class="complete" id="complete">Pour nous aider à comprendre si terminer le cours de mieux-être a apporté des changements au bien-être des personnes, nous aimerions vous posez quelques questions au sujet de votre bien-être.</p>

            <p class="below" id="below">Les résultats de l'échelle de bien-être nous permettront d'utiliser les données agrégées (regroupées) pour savoir si la participation au cours de mieux-être a eu un effet positif sur le bien-être des participantes et des participants. Votre participation à l'échelle de bien-être n'est pas obligatoire.<br>
                Nous avons mis en place des mesures pour protéger vos renseignements et vos données confidentielles.<br><br>
                <input type="checkbox" id="checkbox-res">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; En cochant cette case, vous donnez votre consentement à votre participation telle qu'indiqué ci-dessus.<br><br>
                Les énoncés ci-dessous concernent vos sensations et vos pensées.<br>
                Veuillez cocher la case qui correspond le mieux à votre vécu durant les deux dernières semaines.</p>
            <p class="below" id="below2">L'échelle de bien-être  (WEMWBS) est une mesure de bien-être mental (ce n'est pas un outil diagnostique de dépistage). Un pointage plus élevé indique un niveau supérieur de bien-être avec le total des points allant de 14 à 70.<br>
                Le graphique montre votre niveau de bien-être la première fois et aussi la deuxième fois que vous avez complété le sondage. Ceci vous permet de voir s'il y a eu changement dans votre bien-être. Toute augmentation de plus de 3 points est sans doute significative et indique une amélioration du bien-être.<br><br>
                Vous pouvez utiliser l'information de l'échelle de bien-être pour:<br>
                a) adresser les domaines du bien-être qui bénéficieraient d'une attention et d'un soutien accrus, et<br>
                b) identifier les domaines du bien-être qui se sont renforcés et amliorés à cause de vos efforts.<br><br>
                * Si vous avez des inquiétudes au sujet de votre santé mentale et de votre bien-être mental, veuillez consulter un médicin ou autre responsable des soins de santé.</p>
        </div>

        <div class="container" class="steps" id="steps">
            <div class="step" id="step1"></div>
            <div class="step" id="step2"></div>
            <div class="step" id="step3"></div>
            <div class="step" id="step4"></div>
            <div class="step" id="step5"></div>
            <div class="step" id="step6"></div>
            <div class="step" id="step7"></div>
            <div class="step" id="step8"></div>
            <div class="step" id="step9"></div>
            <div class="step" id="step10"></div>
            <div class="step" id="step11"></div>
            <div class="step" id="step12"></div>
            <div class="step" id="step13"></div>
            <div class="step" id="step14"></div>
        </div>

        <div style="background-color: #EEF5F9; margin-top: 32px; height:260px;" id="answers">
            <div class="container">
                <div class="side side-left">
                </div>
                <div class="side side-right">
                    <div class="sr">
                        <div class="five" id="five1" onclick="choose(this.id);">
                            <div class="f-top" style="padding-top: 8px;">Jamais</div>
                            <div class="f-bottom">1</div>
                        </div>
                        <div class="five" id="five2" onclick="choose(this.id);">
                            <div class="f-top" style="padding-top: 8px;">Rarement</div>
                            <div class="f-bottom">2</div>
                        </div>
                        <div class="five" id="five3" onclick="choose(this.id);">
                            <div class="f-top" style="padding-top: 8px;">Parfois</div>
                            <div class="f-bottom">3</div>
                        </div>
                        <div class="five" id="five4" onclick="choose(this.id);">
                            <div class="f-top" style="padding-top: 8px;">Souvent</div>
                            <div class="f-bottom">4</div>
                        </div>
                        <div class="five" id="five5" onclick="choose(this.id);">
                            <div class="f-top">Tout le temps</div>
                            <div class="f-bottom">5</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="but" id="but-back" onclick="back();" style="width: 160px;">Page précédente</div>
            <div class="but active" id="but-next" onclick="next();" style="width: 160px;">Page suivante</div>
        </div>

        <div id="results" class="container" style="margin-top: 60px;">
            <div class="points">
                <div class="point">
                    <span class="point-sq">5</span>Tout le temps
                </div>
                <div class="point">
                    <span class="point-sq">4</span><span class="low">Souvent</span>
                </div>
                <div class="point">
                    <span class="point-sq">3</span><span class="low">Parfois</span>
                </div>
                <div class="point">
                    <span class="point-sq">2</span><span class="low" style="margin-right: -8px; margin-left: -4px;">Rarement</span>
                </div>
                <div class="point">
                    <span class="point-sq">1</span><span class="low">Jamais</span>
                </div>
            </div>
            <div class="graphic">
                <div style="clear: both;">
                    <canvas id="canvas" width="844px" height="360px" style="clear: both;"></canvas>
                </div>
                <div class="numbers">
                    <div class="number">1</div>
                    <div class="number">2</div>
                    <div class="number">3</div>
                    <div class="number">4</div>
                    <div class="number">5</div>
                    <div class="number">6</div>
                    <div class="number">7</div>
                    <div class="number">8</div>
                    <div class="number">9</div>
                    <div class="number">10</div>
                    <div class="number">11</div>
                    <div class="number">12</div>
                    <div class="number">13</div>
                    <div class="number">14</div>
                </div>
                <div class="please">Veuillez placer votre curseur sur le numéro de la question pour lire la question associée. </div>
                <div class="results">
                    <div class="res" id="first"><span class="res-sq"></span>Résultats du premier test: <span id="first-score"></span></div>
                    <div class="res" id="second"><span class="res-sq"></span>Résultats du deuxième test: <span id="second-score"></span></div>
                </div>

                <div style="margin-top: 150px;" class="q-list"></div>
                <div class="q-list"></div>
                <div class="q-list"></div>
                <div class="q-list"></div>
                <div class="q-list"></div>
                <div class="q-list"></div>
                <div class="q-list"></div>
                <div class="q-list"></div>
                <div class="q-list"></div>
                <div class="q-list"></div>
                <div class="q-list"></div>
                <div class="q-list"></div>
                <div class="q-list"></div>
                <div class="q-list"></div>

            </div>
        </div>
        <div class="container" id="close">
            <!--<div class="but active" id="but-close" onclick="closeData();">CLOSE</div>-->
            <?php
            if($_GET["a"] == 1){
                echo '<a style="width:200px;height: auto;display:block;padding:0 20px;line-height:35px;" class="but active" id="but-close" href="https://wellcentral.ca/vc/mod/scorm/view.php?id=11">TAKE COURSE</a>';
            } else {
                echo '<a style="width:200px;height: auto;display:block;padding:0 20px;line-height:35px;" class="btn btn-primary" id="but-close" href="https://www.surveymonkey.com/r/well-beingcourse2?">TAKE SURVEY</a>';
            }
            ?>
        </div>
    </div>
</div>

<div class="container">
    <p class="below" style="font-size: 14px !important;">L'échelle de bien-être mental de Warwick-Edinburgh ou [WEMWBS] a été développée par l'Université de Warwick avec le concours de NHS Health Scotland, l'Université d'Edinburgh et l'Université de Leeds. © University of Warwick, 2006, tous droits réservés.</p>
</div>

<footer class="foo">
    <div class="cont1200">
        <div class="col1">
            <img src="Logo_footer_1.png" alt="Well Central Logo" style="max-width: 335px">
        </div>
        <div class="col2">
            <img src="../Logo_footer_2.svg" alt="Well Central Logo" style="max-width: 335px">
        </div>
        <div class="col3">
            <ul>
                <li style="margin-right: -1px;"><a href="https://wellcentral.ca/fr/" target="_blank">À PROPOS</a></li>
                <li style="margin-left: -1px; margin-right: -1px;"><a href="https://wellcentral.ca/fr/well-being-course/" target="_blank">LES COURS</a></li>
                <li style="margin-left: -1px; margin-right: -1px;"><a href="https://wellcentral.ca/fr/faqs/" target="_blank">FAQ</a></li>
                <li style="margin-left: -1px;"><a href="https://wellcentral.ca/fr/contact/" target="_blank">NOUS JOINDRE</a></li>
            </ul>
            <div class="col3b">
                <a href="https://symbicore.com/" target="_blank">Conçu et développé par Symbicore</a>
                <span style="padding: 0 20px; color: white; font-weight: bold">||</span>
                <a style="cursor: pointer;" onclick="openPopup();">Avertissement</a>
            </div>
        </div>
    </div>
</footer>

<div id="black-cover">
    <div class="wc-popup">
        <div id="wc-close-popup" onclick="closePopup();">+</div>
        <h1>Avertissement:</h1>
        <p>
            Avis de non-responsabilité – Ce site fournit uniquement des renseignements d’ordre général et pourrait refléter ou ne pas refléter la position de l’Association canadienne pour la santé mentale (ACSM). Les renseignements fournis ne peuvent se substituer à l’avis d’une professionnelle ou d’un professionnel de la santé.  Si vous sentez que vous avez besoin d’un avis médical, veuillez consulter une professionnelle qualifiée ou un professionnel qualifié de la santé. L’ACSM s’efforce d’assurer que les renseignements sont exacts lors de leur publication sur ce site Web. Elle n’offre aucune garantie ou déclaration quant à l’exactitude et la pertinence des renseignements affichés.
        </p>
    </div>
</div>

</body>
</html>