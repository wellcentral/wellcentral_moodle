<!DOCTYPE html>
<html lang="EN">
<head>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="shortcut icon" href="favicon.png" type="image/x-icon" />
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="javascript.js"></script>
    <title>Assessment Tool</title>
</head>
<body>
<?php
error_reporting(0);
//file_put_contents('data', "\n");
if (!empty($_GET['u']) && !empty($_GET['a'])){
    $user = $_GET['u'];
    $assess = $_GET['a'];

    /*
    if(!empty($_GET['c'])){
        if ($_GET['c'] == 1){
            file_put_contents('data', "$user,$assess,9\n", FILE_APPEND | LOCK_EX);
        }
    }
    */

    if($assess == 1 || $assess == 2) {

        $passed = false;
        $passed1 = false;

        $lines = file('data', FILE_IGNORE_NEW_LINES);
        $result = [];
        foreach ($lines as $line) {
            $record = explode(",", $line);
            if ($record[0] == $user) $result[] = $record;
        }

        foreach ($result as $res) {

            if($res[1] == 1 && $assess == 2 && $res[2] != 9){
                $passed1 = true;
            }

            if($res[1] == 1 && $assess == 2 && $res[2] != 9){
                $res1 = $res;
            }
            if ($res[2] != 9 && $res[1] == $assess) {
                $passed = true;

                if($assess == 2){
                    $res2 = $res;
                    $res = $res1;
                    echo "<script>document.addEventListener('DOMContentLoaded', function(event) {graphic();drawLine($res[1],$res[2],$res[3],$res[4],$res[5],$res[6],$res[7],$res[8],$res[9],$res[10],$res[11],$res[12],$res[13],$res[14],$res[15]);});</script>";
                    $res = $res2;
                    echo "<script>document.addEventListener('DOMContentLoaded', function(event) {drawLine($res[1],$res[2],$res[3],$res[4],$res[5],$res[6],$res[7],$res[8],$res[9],$res[10],$res[11],$res[12],$res[13],$res[14],$res[15]);});</script>";
                }
                if($assess == 1) {
                    echo "<script>document.addEventListener('DOMContentLoaded', function(event) {graphic();drawLine($res[1],$res[2],$res[3],$res[4],$res[5],$res[6],$res[7],$res[8],$res[9],$res[10],$res[11],$res[12],$res[13],$res[14],$res[15]);});</script>";
                }
            }
            /*
            if ($res[2] == 9 && $res[1] == $assess) {
                $passed = true;
                echo '<script>document.addEventListener("DOMContentLoaded", function(event) {notFound();});</script>';
            }
            */
        }

        if ($passed == false && !empty($_GET['r1'])) {
            $r0 = $assess;
            $r1 = $_GET['r1'];
            $record = "$user,$r0,$r1";
            if (!empty($_GET['r2']) && !empty($_GET['r3']) && !empty($_GET['r4']) && !empty($_GET['r5']) && !empty($_GET['r6']) && !empty($_GET['r7']) && !empty($_GET['r8']) && !empty($_GET['r9']) && !empty($_GET['r10']) && !empty($_GET['r11']) && !empty($_GET['r12']) && !empty($_GET['r13']) && !empty($_GET['r14']) && !empty($_GET['r15'])) {
                $r2 = $_GET['r2'];
                $r3 = $_GET['r3'];
                $r4 = $_GET['r4'];
                $r5 = $_GET['r5'];
                $r6 = $_GET['r6'];
                $r7 = $_GET['r7'];
                $r8 = $_GET['r8'];
                $r9 = $_GET['r9'];
                $r10 = $_GET['r10'];
                $r11 = $_GET['r11'];
                $r12 = $_GET['r12'];
                $r13 = $_GET['r13'];
                $r14 = $_GET['r14'];
                $r15 = $_GET['r15'];
                $record .= ",$r2,$r3,$r4,$r5,$r6,$r7,$r8,$r9,$r10,$r11,$r12,$r13,$r14,$r15";
            }
            $record .= "\n";
            file_put_contents('data', $record, FILE_APPEND | LOCK_EX);
            echo "<script>document.addEventListener('DOMContentLoaded', function(event) {redir('$user','$assess');});</script>";
        }
    }
    else{
        echo '<script>document.addEventListener("DOMContentLoaded", function(event) {notFound();});</script>';
    }
    //$record = "gh@df.com,1,1,3,3,1,2,1,2,2,3,1,4,5,3,2\n";
    //file_put_contents('data', ' ');
    //file_put_contents('data', $record, FILE_APPEND | LOCK_EX);
}
else{
    echo '<script>document.addEventListener("DOMContentLoaded", function(event) {notFound();});</script>';
}

if($passed1 == false && $assess == 2){
    echo '<script>document.addEventListener("DOMContentLoaded", function(event) {notFound();});</script>';
}
else{
    file_put_contents('data', "$user,$assess,9\n", FILE_APPEND | LOCK_EX);
}


?>

<div class="wrap">

    <div class="site-index">
        <div class="jumbotron container">
            <a href="https://cmhawellcentral.dev.symbicore.com/" target="_blank"><img src="Well_Central_Logo.png" alt="Well Central Logo"></a>
            <h1 id="hh">The Warwick-Edinburgh Mental Well-being Scale (WEMWBS)</h1>

            <p class="complete" id="complete">To help us understand if completing the Well-being Course has made any changes to people’s wellbeing, we would like you to answer some questions about your wellbeing.</p>

            <p class="below" id="below">Results from the well-being scale will be used as aggregate (grouped) data to determine if participation in the well-being course has had a positive impact on participants' well-being. Your completion of the well-being scale is voluntary.<br>
                We have taken measures to protect your information and confidentiality.<br><br>
                <input type="checkbox" id="checkbox-res">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; By checking this box, you are providing consent to participate as indicated above.<br><br>
                Below are some statements about feelings and thoughts.<br>
                Please tick the box that best describes your experience of each over the last 2 weeks.</p>
            <p class="below" id="below2">The Well-being Scale (WEMWBS) is a measure of mental well-being; (it is not a diagnostic screening tool). A higher score indicates a greater level of well-being with total scores ranging between 14 and 70.<br>
                The graph shows your well-being scores from both time one and time two of completing the scale. This allows you to see if any change has occurred.  Any increase of more than three points is likely to be meaningful and indicate an improvement in well-being.<br><br>
                You can use the information from the well-being scale to<br>
                a) address areas of well-being that would benefit from further attention and support and<br>
                b) recognize areas of well-being that have strengthened and improved as a result of your efforts.<br><br>
                *If you are concerned about your mental health and well-being please consult a doctor or other healthcare practitioner.</p>
        </div>

        <div class="container" class="steps" id="steps">
            <div class="step" id="step1"></div>
            <div class="step" id="step2"></div>
            <div class="step" id="step3"></div>
            <div class="step" id="step4"></div>
            <div class="step" id="step5"></div>
            <div class="step" id="step6"></div>
            <div class="step" id="step7"></div>
            <div class="step" id="step8"></div>
            <div class="step" id="step9"></div>
            <div class="step" id="step10"></div>
            <div class="step" id="step11"></div>
            <div class="step" id="step12"></div>
            <div class="step" id="step13"></div>
            <div class="step" id="step14"></div>
        </div>

        <div style="background-color: #EEF5F9; margin-top: 32px; height:260px;" id="answers">
            <div class="container">
                <div class="side side-left">
                </div>
                <div class="side side-right">
                    <div class="sr">
                        <div class="five" id="five1" onclick="choose(this.id);">
                            <div class="f-top">None of the time</div>
                            <div class="f-bottom">1</div>
                        </div>
                        <div class="five" id="five2" onclick="choose(this.id);">
                            <div class="f-top" style="padding-top: 8px;">Rarely</div>
                            <div class="f-bottom">2</div>
                        </div>
                        <div class="five" id="five3" onclick="choose(this.id);">
                            <div class="f-top">Some of the time</div>
                            <div class="f-bottom">3</div>
                        </div>
                        <div class="five" id="five4" onclick="choose(this.id);">
                            <div class="f-top" style="padding-top: 8px;">Often</div>
                            <div class="f-bottom">4</div>
                        </div>
                        <div class="five" id="five5" onclick="choose(this.id);">
                            <div class="f-top">All of the time</div>
                            <div class="f-bottom">5</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="but" id="but-back" onclick="back();">BACK</div>
            <div class="but active" id="but-next" onclick="next();">NEXT</div>
        </div>

        <div id="results" class="container" style="margin-top: 60px;">
            <div class="points">
                <div class="point">
                    <span class="point-sq">5</span>All of the time
                </div>
                <div class="point">
                    <span class="point-sq">4</span><span class="low">Often</span>
                </div>
                <div class="point">
                    <span class="point-sq">3</span>Some of the time
                </div>
                <div class="point">
                    <span class="point-sq">2</span><span class="low">Rarely</span>
                </div>
                <div class="point">
                    <span class="point-sq">1</span>None of the time
                </div>
            </div>
            <div class="graphic">
                <div style="clear: both;">
                    <canvas id="canvas" width="844px" height="360px" style="clear: both;"></canvas>
                </div>
                <div class="numbers">
                    <div class="number">1</div>
                    <div class="number">2</div>
                    <div class="number">3</div>
                    <div class="number">4</div>
                    <div class="number">5</div>
                    <div class="number">6</div>
                    <div class="number">7</div>
                    <div class="number">8</div>
                    <div class="number">9</div>
                    <div class="number">10</div>
                    <div class="number">11</div>
                    <div class="number">12</div>
                    <div class="number">13</div>
                    <div class="number">14</div>
                </div>
                <div class="please">Please place your cursor over the question number to see the associated question.</div>
                <div class="results">
                    <div class="res" id="first"><span class="res-sq"></span>First test result: <span id="first-score"></span></div>
                    <div class="res" id="second"><span class="res-sq"></span>Second test result: <span id="second-score"></span></div>
                </div>

                <div style="margin-top: 150px;" class="q-list"></div>
                <div class="q-list"></div>
                <div class="q-list"></div>
                <div class="q-list"></div>
                <div class="q-list"></div>
                <div class="q-list"></div>
                <div class="q-list"></div>
                <div class="q-list"></div>
                <div class="q-list"></div>
                <div class="q-list"></div>
                <div class="q-list"></div>
                <div class="q-list"></div>
                <div class="q-list"></div>
                <div class="q-list"></div>

            </div>
        </div>
        <div class="container" id="close">
            <!--<div class="but active" id="but-close" onclick="closeData();">CLOSE</div>-->
            <?php
                if($_GET["a"] == 1){
                    echo '<a style="width:200px;height: auto;display:block;padding:0 20px;line-height:35px;" class="but active" id="but-close" href="https://wellcentral.ca/vc/mod/scorm/view.php?id=11">TAKE COURSE</a>';
                } else {
                    echo '<a style="width:200px;height: auto;display:block;padding:0 20px;line-height:35px;" class="btn btn-primary" id="but-close" href="https://www.surveymonkey.com/r/well-beingcourse2?">TAKE SURVEY</a>';
                }
            ?>
        </div>
    </div>
</div>

<div class="container">
    <p class="below" style="font-size: 14px !important;">The Warwick-Edinburgh Wellbeing Scale or [WEMWBS] was developed by the University of Warwick in conjunction with NHS Health Scotland, University of Edinburgh and the University of Leeds. ©University of Warwick, 2006, all rights reserved.</p>
</div>

<footer class="foo">
    <div class="cont1200">
        <div class="col1">
            <img src="Logo_footer_1.png" alt="Well Central Logo" style="max-width: 335px">
        </div>
        <div class="col2">
            <img src="Logo_footer_2.svg" alt="Well Central Logo" style="max-width: 335px">
        </div>
        <div class="col3">
            <ul>
                <li><a href="https://wellcentral.ca/" target="_blank">ABOUT</a></li>
                <li><a href="https://wellcentral.ca/well-being-course/" target="_blank">COURSE</a></li>
                <li><a href="https://wellcentral.ca/faqs/" target="_blank">FAQ</a></li>
                <li><a href="https://wellcentral.ca/contact/" target="_blank">CONTACT</a></li>
            </ul>
            <div class="col3b">
                <a href="https://symbicore.com/" target="_blank">Designed and Developed by Symbicore</a>
                <span style="padding: 0 20px; color: white; font-weight: bold">||</span>
                <a style="cursor: pointer;" onclick="openPopup();">Disclaimer</a>
            </div>
        </div>
    </div>
</footer>

<div id="black-cover">
    <div class="wc-popup">
        <div id="wc-close-popup" onclick="closePopup();">+</div>
        <h1>Disclaimer:</h1>
        <p>
            This site provides general information only and may or may not reflect the position of the Canadian Mental Health Association (CMHA). Information provided is not a substitute for professional advice. If you feel that you may need medical advice, please consult a qualified health care professional. CMHA makes every reasonable effort to ensure that the information is accurate at the time of posting. We cannot guarantee the reliability of any information posted.
        </p>
    </div>
</div>

</body>
</html>