<?php

require_once("classes/ac_connector/curl_requests.php");
require_once("classes/ac_connector/run_automation.php");

function local_reminders_cron_task() {

    global $DB;

    //    get all users from moodle db that have not logged in for a week
    $sendusers = $DB->get_records_sql("SELECT *
        FROM {user}
        WHERE id > 1 AND deleted=0 AND suspended=0 AND confirmed=1;");
    $mail_list = array();
    foreach($sendusers as $value) {
        if(($value->lastaccess + (7 * 24 * 60 * 60)) < time()) {
            $mail_list[] = $value->email;
        }
    }

    $run_automation = new run_automation(2, $mail_list, '');
    $run_automation->run_automation();

}

function send_reminders_uncompleted_course() {

    global $DB;

    //    get all users from moodle db that have not logged in for a week
    $sendusers = $DB->get_records_sql("SELECT DISTINCT u.email
        FROM {user} u
        INNER JOIN {course_modules_completion} cmc ON u.id=cmc.userid
        WHERE completionstate=0");
    $mail_list = array();
    foreach($sendusers as $value) {
        $mail_list[] = $value->email;
    }

    $run_automation = new run_automation(3, $mail_list, '');
    $run_automation->run_automation();

}
