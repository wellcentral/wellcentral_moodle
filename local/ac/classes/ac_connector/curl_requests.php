<?php

class Curl_requests {

    public $api_url = 'https://wellcentral.api-us1.com/api/3/';
    public $headers = array('Api-Token: 41549386b7613c07bc01a3a9dcb5c6d94bb422e9a9b24c1059751dfb590e2614ec2097cf');

    function __construct($url, $json) {
        $this->url = $url;
        $this->json = $json;
    }

    public function post_request() {
        $curl = curl_init($this->api_url . $this->url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $this->json);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $this->headers);
        $curl_response = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $curl_response_decoded = json_decode(curl_exec($curl), true);
        curl_close($curl);
        var_dump($curl_response);
        var_dump($curl_response_decoded);
    }

    public function get_request() {
        $curl = curl_init($this->api_url);
        curl_setopt($curl, CURLOPT_URL, $this->api_url . $this->url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $this->headers);
        $curl_response = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $curl_response_decoded = json_decode(curl_exec($curl), true);
        curl_close($curl);
        return $curl_response_decoded;
    }

    public function delete_request() {
        $curl = curl_init($this->api_url);
        curl_setopt($curl, CURLOPT_URL, $this->api_url . $this->url);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($curl, CURLOPT_POSTFIELDS, $this->json);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Api-Token: 41549386b7613c07bc01a3a9dcb5c6d94bb422e9a9b24c1059751dfb590e2614ec2097cf'));
        $curl_response = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $curl_response_decoded = json_decode(curl_exec($curl), true);
        curl_close($curl);
        var_dump($curl_response);
        var_dump($curl_response_decoded);
    }

}