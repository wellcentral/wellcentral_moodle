<?php

require_once("curl_requests.php");

class run_automation {

    function __construct($automation_id, $mail_list) {
        $this->automation_id = $automation_id;
        $this->mail_list = $mail_list;
//        $this->sendusers = $sendusers;
    }

    public function run_automation() {
        //    get list of all automations a contact is in
        $all_automations_contact = new Curl_requests('contactAutomations', '');
        $all_automations_contacts_list = $all_automations_contact->get_request();

        //   remove all contacts from automation
        foreach($all_automations_contacts_list["contactAutomations"] as $value) {
            $curl_requests = new Curl_requests('contactAutomations/' . $value['id'], '');
            $curl_requests->delete_request();
        }

        //    get list of all contacts
        $curl_requests = new Curl_requests('contacts?status=-1&orders%5Bemail%5D=ASC', '');
        $all_contacts_list = $curl_requests->get_request();

        //    add all users that have not logged in for a week to automation
        foreach($all_contacts_list['contacts'] as $value) {
            if(in_array($value['email'], $this->mail_list) && $value['email'] != 'tools@symbicore.com') {
                $add_contact_to_automation_json = '{
                "contactAutomation": {
                    "contact": "' . $value['id'] . '",
                    "automation": "' . $this->automation_id . '"
                }
            }';
                $add_contact_to_automation = new Curl_requests('contactAutomations', $add_contact_to_automation_json);
                $add_contact_to_automation->post_request();
            }
        }

        //    add trigger contact to automation
        $trigger_contact = '{
           "contactAutomation": {
              "contact": "54",
              "automation": "' . $this->automation_id . '"
           }
        }';
        $add_trigger_contact = new Curl_requests('contactAutomations', $trigger_contact);
        $add_trigger_contact->post_request();
    }

//    public function add_users_from_moodle_to_active_campaign() {
//        //  add all contacts from moodle db if necessary
//        foreach($this->sendusers as $value) {
//        $contact = '{
//            "contact": {
//                "email": "' . $value->email . '",
//                "firstName": "' . $value->firstname . '",
//                "lastName": "' . $value->lastname . '",
//                "phone": "' . $value->phone1 . '",
//                "fieldValues":[
//                  {
//                    "field":"1",
//                    "value":"The Value for First Field"
//                  },
//                  {
//                    "field":"6",
//                    "value":"2008-01-20"
//                  }
//                ]
//            }
//        }';
//        $add_trigger_contact = new Curl_requests('contacts', $contact);
//        $add_trigger_contact->post_request();
//        }
//    }
}